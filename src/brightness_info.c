#define _DEFAULT_SOURCE

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>

#include "brightness_info.h"

#define STRING_LEN 32
#define SLEEP_MICROSECONDS 100000

static char* output_string = NULL;
static pthread_mutex_t output_lock;
static int initialized = 0;
static pthread_t thread;

static void* query_brightness_thread() {
    
    if( output_string == NULL ) {
        output_string = calloc( STRING_LEN, sizeof(char) );
    }

    while( initialized ) {
        FILE *fp;
        char brightness[8];

        fp = fopen("/sys/class/backlight/acpi_video0/brightness", "r");
        fscanf( fp, "%s\n", brightness );
        fclose(fp);

        pthread_mutex_lock( &output_lock );
        strncpy( output_string, "Brightness: ", STRING_LEN-1 );
        strncat( output_string, brightness, STRING_LEN-1 );
        pthread_mutex_unlock( &output_lock );

        usleep( SLEEP_MICROSECONDS );
    }

    free( output_string );
    output_string = NULL;

    return NULL;
}

int brightness_init() {
    
    if( initialized ) return 0;

    pthread_mutex_init( &output_lock, NULL );
    pthread_create( &thread, NULL, &query_brightness_thread, NULL );

    initialized = 1;
    return 1;
}

int brightness_destroy() {

    if( !initialized ) return 0;
    initialized = 0;

    pthread_join( thread, NULL );
    pthread_mutex_destroy( &output_lock );
    
    return 1;
}

char* get_brightness_information() {
    
    if( !initialized ) return NULL;

    char* brightness_string = calloc( STRING_LEN, sizeof(char) );

    pthread_mutex_lock( &output_lock );
    if( output_string )
        strncpy( brightness_string, output_string, STRING_LEN-1 );
    pthread_mutex_unlock( &output_lock );

    return brightness_string;
}
