#define _DEFAULT_SOURCE

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

#include "errors.h"
#include "desktop_info.h"

#define STRING_LEN 512
#define SLEEP_MICROSECONDS 100000

static char* output_string = NULL;
static pthread_mutex_t output_lock;
static int initialized = 0;
static pthread_t thread;

static void* query_desktop_thread() {

    if( output_string == NULL ) {
        output_string = calloc( STRING_LEN, sizeof(char) );
    }

    FILE *bspcfp;
    char *desktop_status;
    char* to_free;

    while( initialized ) {

        desktop_status = calloc( 64, sizeof(char) );
        to_free = desktop_status; // pointer to original loc so we can free later

        bspcfp = popen("bspc wm --get-status", "r");
        if( bspcfp == NULL ) {
            pthread_mutex_lock( &output_lock );
            strncpy( output_string, "Could not fetch display.", STRING_LEN-1 );
            pthread_mutex_unlock( &output_lock );

            free(to_free);
        }
        else {
            fscanf( bspcfp, "%s\n", desktop_status );
            pclose( bspcfp );

            char* token;

            token = strsep( &desktop_status, ":"); // Remove first group.

            pthread_mutex_lock( &output_lock );

            memset( output_string, 0, STRING_LEN );

            for( int i = 1; i <= 6; i++ ) {

                if( (token = strsep(&desktop_status, ":")) != NULL ) {
                    char state = token[0];
                    char color[8] = "#131313"; // default
                    if( !state_to_color(color, state) ) {
                        log_error("Invalid desktop state!");
                    }

                    sprintf( output_string+strnlen(output_string, STRING_LEN-1), "%%{F%s}%%{A:bspc desktop -f %d | sh:} %d %%{A}%%{F-}", color, i, i ); // TODO : Should this be strncat instead?
                }

            }

            pthread_mutex_unlock( &output_lock );

            free(to_free);
        }

        usleep( SLEEP_MICROSECONDS );
    }
    
    free( output_string );
    output_string = NULL;
    
    return NULL;
}

int desktop_init() {
    
    if( initialized ) return 0;

    pthread_mutex_init( &output_lock, NULL );
    pthread_create( &thread, NULL, &query_desktop_thread, NULL );

    initialized = 1;
    return 1;
}

int desktop_destroy() {

    if( !initialized ) return 0;
    initialized = 0;

    pthread_join(thread, NULL );
    pthread_mutex_destroy( &output_lock );
    
    return 1;
}

char* get_desktop_display() {

    if( !initialized ) return NULL;

    char* desktop_string = calloc( STRING_LEN, sizeof(char) );

    pthread_mutex_lock( &output_lock );
    if( output_string ) {
        strncpy( desktop_string, output_string, STRING_LEN-1 );
    }
    pthread_mutex_unlock( &output_lock );

    return desktop_string;
}

static int state_to_color( char* colorDest, char state ) {
    switch( state ) {
        case 'o':
            strncpy( colorDest, "#FF0000", 7 );
            break;
        case 'f':
            strncpy( colorDest, "#FFFFFF", 7 );
            break;
        case 'O':
        case'F':
            strncpy( colorDest, "#00FF00", 7 );
            break;
        default:
            strncpy( colorDest, "#131313", 7 );
            return 0;
            break;
    }

    return 1;
}
