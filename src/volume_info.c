#define _DEFAULT_SOURCE

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

#include "errors.h"
#include "volume_info.h"

#define STRING_LEN 128
#define SLEEP_MICROSECONDS 20000

static char* output_string = NULL;
static pthread_mutex_t output_lock;
static int initialized = 0;
static pthread_t thread;

static void* query_volume_thread() {

    if( output_string == NULL ) {
        output_string = calloc( STRING_LEN, sizeof(char) );
    }

    while( initialized ) {
        FILE *acpifp;

        acpifp = popen("pulseaudio-ctl full-status 2> /dev/null", "r");
        if( acpifp == NULL ) {
            strncpy( output_string, "Volume: --%", 100 );
        }
        else {
            int volume_level = -1;
            char mute_string[4];
            int muted = 0;

            fscanf( acpifp, "%d %s", &volume_level, mute_string );
            pclose( acpifp );

            if( strcmp( mute_string, "yes" ) == 0 ) {
                muted = 1;
            }

            if( volume_level == -1 ) {
                log_error("Failed to fetch volume level.");
            }

            pthread_mutex_lock( &output_lock );

            memset( output_string, 0, STRING_LEN );
            sprintf( output_string, "Volume: %d%%%s", volume_level, muted ? " MUTE" : "" );

            pthread_mutex_unlock( &output_lock );
        }

        usleep( SLEEP_MICROSECONDS );
    }

    free( output_string );
    output_string = NULL;

    return NULL;
}

int volume_init() {

    if( initialized ) return 0;

    pthread_mutex_init( &output_lock, NULL );
    pthread_create( &thread, NULL, &query_volume_thread, NULL );

    initialized = 1;
    return 1;
}

int volume_destroy() {
    
    if( !initialized ) return 0;
    initialized = 0;

    pthread_join( thread, NULL );
    pthread_mutex_destroy( &output_lock );

    return 1;
}

char* get_volume_information() {

    if( !initialized ) return NULL;

    char* volume_string = calloc( STRING_LEN, sizeof(char) );

    pthread_mutex_lock( &output_lock );
    if( output_string ) {
        strncpy( volume_string, output_string, STRING_LEN-1 );
    }
    pthread_mutex_unlock( &output_lock );

    return volume_string;
}
