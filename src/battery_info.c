#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <pthread.h>
#include <unistd.h>

#include "errors.h"
#include "battery_info.h"

#define STRING_LEN 32
#define SLEEP_SECONDS 2

static char* output_string = NULL;
static pthread_mutex_t output_lock;
static int initialized = 0;
static pthread_t thread;
static int has_battery = 0;

static void* query_battery_thread() {

    FILE *fp;

    if( output_string == NULL ) {
        output_string = calloc( STRING_LEN, sizeof(char) );
    }

    if( !has_battery ) {
        pthread_mutex_lock( &output_lock );
        strncpy( output_string, "100% **", STRING_LEN-1 );
        pthread_mutex_unlock( &output_lock );
        return NULL;
    }

    while( initialized ) {

        char charge_status[STRING_LEN];

        pthread_mutex_lock( &output_lock );

        fp = fopen( "/sys/class/power_supply/BAT0/capacity", "r" );
        fscanf( fp, "%s", output_string );
        strncat( output_string, "% ", STRING_LEN-1 ); // Add the percent symbol
        fclose(fp);

        fp = fopen( "/sys/class/power_supply/BAT0/status", "r" );
        fscanf( fp, "%s", charge_status );
        fclose(fp);

        if ( strcmp( charge_status, "Charging" ) == 0 ) {
            strncat( output_string, "++", STRING_LEN-1 );
        }
        else if ( strcmp( charge_status, "Discharging" ) == 0 ) {
            strncat( output_string, "--", STRING_LEN-1 );
        }
        else {
            strncat( output_string, "??", STRING_LEN-1 );
            log_error( "Failed to fetch charging status." );
        }

        pthread_mutex_unlock( &output_lock );
        
        sleep( SLEEP_SECONDS );
    }

    free( output_string );
    output_string = NULL;

    return NULL;
}

int battery_init() {
    if( initialized ) return 0;

    pthread_mutex_init( &output_lock, NULL );
    pthread_create( &thread, NULL, &query_battery_thread, NULL );
    
    // If there is no BAT0 directory, this is a desktop.
    struct stat s;
    if( stat("/sys/class/power_supply/BAT0", &s) == -1 ) {
        has_battery = 0;
    }
    else {
        has_battery = 1;
    }

    initialized = 1;
    return 1;
}

int battery_destroy() {
    if( !initialized ) return 0;
    initialized = 0;

    pthread_join( thread, NULL );
    pthread_mutex_destroy( &output_lock );

    return 1;
}

/**
 * return: "50% --" for 50% charged and discharging
 */
char* get_battery_information() {

    if( !initialized ) return NULL;

    char* battery_string = calloc( STRING_LEN, sizeof(char) );
    
    pthread_mutex_lock( &output_lock );
    if( output_string )
        strncpy( battery_string, output_string, STRING_LEN-1 );
    pthread_mutex_unlock( &output_lock );

    return battery_string;

}
