char* get_desktop_display();
static void* query_desktop_thread();
int desktop_init();
int desktop_destroy();
static int state_to_color( char* colorDest, char state );
