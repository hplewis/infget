#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <pthread.h>
#include <string.h>

#include "datetime_info.h"
#include "tmformat.h"

#define STRING_LEN 64
#define SLEEP_SECONDS 1

static char* output_string = NULL;
static pthread_mutex_t output_lock;
static int initialized = 0;
static pthread_t thread;


static void* query_time_thread() {

    time_t raw;
    struct tm* info;

    if(output_string == NULL) {
        output_string = calloc( STRING_LEN, sizeof(char) );
    }

    while( initialized ) {

        pthread_mutex_lock( &output_lock );

        time(&raw);
        info = localtime(&raw);
        sprintf( output_string, "%s, %s %d, %d %02d:%02d:%02d",
                weekday( info->tm_wday ), month( info->tm_mon ),
                info->tm_mday, info->tm_year+1900, info->tm_hour,
                info->tm_min, info->tm_sec );

        pthread_mutex_unlock( &output_lock );

        sleep( SLEEP_SECONDS );
    }

    free(output_string);
    output_string = NULL;

    return NULL;
}

int datetime_init() {

    if( initialized ) return 0;
    
    pthread_mutex_init( &output_lock, NULL );// maybe do a check for retval here?
    pthread_create( &thread, NULL, &query_time_thread, NULL );

    initialized = 1;
    return 1;
}

int datetime_destroy() {

    if( !initialized ) return 0;
    initialized = 0;

    pthread_join( thread, NULL );
    pthread_mutex_destroy( &output_lock );

    return 1;
}

/**
 * Example: Wednesday, Apr 13, 2016 01:01:26
 */
char* get_datetime_information() {

    if( !initialized ) return NULL;

    char* datetime_string = calloc( STRING_LEN, sizeof(char) );

    pthread_mutex_lock( &output_lock );
    if( output_string ) {
        strncpy( datetime_string, output_string, STRING_LEN-1 );
    }
    pthread_mutex_unlock( &output_lock );

    return datetime_string;

}
