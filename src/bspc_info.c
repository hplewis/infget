#define _DEFAULT_SOURCE

#include <pthread.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>

#define SLEEP_SECONDS 2

static int running = 1;
static pthread_mutex_t running_lock;
static int initialized = 0;
static pthread_t thread;

static void* query_running_thread() {

    while( initialized ) {
        FILE* bspcfp;
        char output[128];

        bspcfp = popen("bspc query 2>&1", "r");
        fscanf( bspcfp, "%s", output );
        pclose( bspcfp );

        pthread_mutex_lock( &running_lock );
        if( strcmp( output, "Failed" ) == 0 ) {
            running = 0;
        }
        else {
            running = 1;
        }
        pthread_mutex_unlock( &running_lock );

        sleep( SLEEP_SECONDS );
    }

    running = 0;

    return NULL;
}

int bspc_init() {
    if( initialized ) return 0;

    pthread_mutex_init( &running_lock, NULL );
    pthread_create( &thread, NULL, &query_running_thread, NULL );

    initialized = 1;
    return 1;
}

int bspc_destroy() {

    if( !initialized ) return 0;
    initialized = 0;

    pthread_join( thread, NULL );
    pthread_mutex_destroy( &running_lock );

    return 1;
}

int is_bspc_running() {

    if( !initialized ) return -1;

    int running_status = -1;

    pthread_mutex_lock( &running_lock );
    running_status = running;
    pthread_mutex_unlock( &running_lock );

    return running_status;

}
