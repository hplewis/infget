#include <stdio.h>
#include <pthread.h>

#include "errors.h"

#define LOG_ERRORS 0

static pthread_mutex_t log_lock;
static int initialized = 0;

/**
 * Log a given error to errors.log
 * @param error - the error string to log
 */
void log_error( char* error ) {
    if( LOG_ERRORS ) {
        FILE *logfp;
        if( !initialized) {
            pthread_mutex_init( &log_lock, NULL );
            initialized = 1;
        }

        pthread_mutex_lock( &log_lock );

        logfp = fopen( "errors.log", "a" );
        fprintf( logfp, "%s\n", error );
        fclose( logfp );

        pthread_mutex_unlock( &log_lock );
    }
}
