#define _DEFAULT_SOURCE

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#include "infget.h"
#include "battery_info.h"
#include "datetime_info.h"
#include "desktop_info.h"
#include "volume_info.h"
#include "brightness_info.h"
#include "bspc_info.h"

#define SLEEP_MICROSECONDS 20000

int main() {

    bspc_init();
    datetime_init();
    battery_init();
    brightness_init();
    desktop_init();
    volume_init();

    while ( is_bspc_running() ) {

        // TODO : Might need to increase due to hidden formatting strings.
        char* status_information = calloc( 1024, sizeof(char) );

        // Left justify.
        strncat( status_information, "%{l}", 1000 );

        // Get desktop display
        char* desktop_info = get_desktop_display();
        strncat( status_information, desktop_info, 1000 );
        free( desktop_info );

        // Right justify.
        strncat( status_information, "%{r}", 1000 );

        // Get brightness info
        char* brightness_info = get_brightness_information();
        strncat( status_information, brightness_info, 1000 );
        free( brightness_info );

        strncat( status_information, " | ", 1000 );

        // Get volume info
        char* volume_info = get_volume_information();
        strncat( status_information, volume_info, 1000 );
        free( volume_info );

        strncat( status_information, " | ", 1000 );

        // Get date and time info
        char* datetime_info = get_datetime_information();
        strncat( status_information, datetime_info, 1000 );
        free( datetime_info );

        strncat( status_information, " | ", 1000 );

        // Get battery info
        char* battery_info = get_battery_information();
        strncat( status_information, battery_info, 1000 );
        free( battery_info );

        // Print output to stdout
        printf( "%s\n", status_information );
        fflush( stdout ); // Flush output to lemonbar

        free( status_information );

        usleep( SLEEP_MICROSECONDS );

    }
    
    bspc_destroy();
    datetime_destroy();
    battery_destroy();
    brightness_destroy();
    desktop_destroy();
    volume_destroy();

    return 0;
    
}
