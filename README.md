# infget [![Build Status](https://travis-ci.org/hplewis/infget.svg?branch=master)](https://travis-ci.org/hplewis/infget)
An information retriever for [lemonbar](https://github.com/LemonBoy/bar).

Retrieves system information and displays the information to standard out
on one line. Each update of information is displayed on a new line. Designed
to use with lemonbar.
